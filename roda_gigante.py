# Enunciado ------------------------------------------------------------------- //

#   Na cidade de Canavieiro do Leste, chegou o parque de diversões "O Palhaço Louco".
#   Uma das atrações é a roda gigante. Na roda gigante, só podem entrar pessoas com dois sobrenomes,
#   mais de 1,61 de aaltura e que não estajam sozinhas.
#   Porém, se uma pessoa tiver exatamente 1,50m e trouxer 4 acompanhantes, pode entrar.
#   Além disso, as cabines são numeradas e se a pessoa entrar na cabine de numero 10
#   e for a centésima pessoa a fazer isso, ganha o emprego do Palhaço Louco.

#   Fornecido o Contexto acima, elabore um algoritimo na abordagem top-down se uma pessoa
#   pode ou não entrar no brinquedo e se ela foi premiada.



# Functions ------------------------------------------------------------------- //

def checkRules(name, height, company):
    # checks if name, height and company count match the rules

    # return True if user can enter thr Ferris Wheel
    # return  False if user can't enter the Ferris Wheel
    return 0

def winner(valid)
    # if person is valid, count the carts

    # return True if the person is the winner
    # return False if person is not the winner
    return 0

def permissionMessage(isValid, isWinner):
    # if person is valid and is not winner, print "Você pode entrar na Roda Gigante"
    # if person is valid and is winner, print "Você ganhou o emprego do Palhaço Louco!!!!"
    # if person is not valid, print "Você não pode entrar na Roda Gigante :("

    return 0    



# Main Code ------------------------------------------------------------------- //

print('Bem vindo ao parque "O Palhaço Louco!"')
print('Para saber se você pode entrar na Roda Gigante, informe seu nome completo, sua altura e a quantidade de acompanhantes')

person_name     = input('informe seu nome:')
person_height   = input('informe sua altura:')
person_company  = input('informe quantos acompanhantes:')

valid = checkRules(person_name, person_height, person_company)
winner = checkWinner(valid)

permissionMessage(valid, winner)